package com.example.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int currentPlayer = 0; //0 bhaneko cross ani 1 bhaneko aalu

    boolean isGameActive = true;
    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2}; // 2 bhaneko chai nakheleko spaces

    int[][] winningPositions = {{0,1,2}, {3,4,5}, {6,7,8}, {0,3,6}, {1,4,7}, {2,5,8}, {0,4,8}, {2,4,6}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void dropIn(View view) {

        ImageView cross = (ImageView) view;

        cross.getTag();

        int tappedSpace = Integer.parseInt(cross.getTag().toString());

        if (gameState[tappedSpace] == 2 && isGameActive) {

            gameState[tappedSpace] = currentPlayer;
            cross.setTranslationY(-1000f);   //taking to the top of the screen

            if (currentPlayer == 0) {
                cross.setImageResource(R.drawable.ic_clear_black_24dp);
                currentPlayer = 1;
            } else {
                cross.setImageResource(R.drawable.ic_brightness_1_black_24dp);
                currentPlayer = 0;
            }
            cross.animate().translationYBy(1000f).rotation(360).setDuration(300);

            for (int [] winningPosition : winningPositions) {
                if(gameState[winningPosition[0]]==gameState[winningPosition[1]] &&
                        gameState[winningPosition[1]]== gameState[winningPosition[2]] &&
                gameState[winningPosition[0]] != 2){


                    isGameActive = false;
                    //winner specified
                    String winner = "Circle";

                    if(gameState[winningPosition[0]] == 0){
                       winner = "Cross";
                    }

                    TextView winnerMessage = (TextView) findViewById(R.id.tv_winner);
                    winnerMessage.setText(winner + " has won!!!");

                    LinearLayout layout = (LinearLayout) findViewById(R.id.linearlayoutad);
                    layout.setVisibility(View.VISIBLE);

                } else{
                    boolean gameIsOver = true;
                    for (int drawState: gameState){
                         if(drawState == 2) gameIsOver = false;
                    }

                    if(gameIsOver) {
                        TextView winnerMessage = (TextView) findViewById(R.id.tv_winner);
                        winnerMessage.setText("It's a Draw!!!!");

                        LinearLayout layout = (LinearLayout) findViewById(R.id.linearlayoutad);
                        layout.setVisibility(View.VISIBLE);

                    }
                }
            }
        }
    }

    public void playAgain(View view) {

        isGameActive = true;
        LinearLayout layout = (LinearLayout) findViewById(R.id.linearlayoutad);
        layout.setVisibility(View.INVISIBLE);


         currentPlayer = 0;

        for (int i =0 ; i < gameState.length; i++){
            gameState[i] = 2 ;
        }

        GridLayout gridLayout = (GridLayout) findViewById(R.id.grid);

        for(int i = 0 ; i < gridLayout.getChildCount(); i++){
            ((ImageView) gridLayout.getChildAt(i)).setImageResource(0);
        }
    }
}
